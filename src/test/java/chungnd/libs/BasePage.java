package chungnd.libs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage extends PageFactory {
    public WebDriver driver;
    WebDriverWait wait;
    Actions actions;
    String text;
    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
        actions = new Actions(driver);
        wait = new WebDriverWait(driver,60);
    }

//    Wait libraries


//  base functions libraries
    public BasePage open(String url){
        driver.get(url);
        return this;
    }
    public BasePage input(String text){
        this.text = text;
        return this;
    }
    public BasePage into(WebElement element){
        element.sendKeys(text);
        return this;
    }

    public BasePage clickOn(WebElement element){
        element.click();
        return this;
    }
    public BasePage doubleClickOn(WebElement element){
        actions.doubleClick(element);
        return this;
    }

}
