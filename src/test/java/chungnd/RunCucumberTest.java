package chungnd;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"json:target/json-report/cucumber.json"},
        glue = "chungnd/steps",
//        tags = "",
        features = "src/test/resources/features"
)
public class RunCucumberTest {

}
