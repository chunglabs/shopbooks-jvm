package chungnd.steps;

import chungnd.pageobjects.HomePage;
import chungnd.pageobjects.LoginForm;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class HomePageSteps {
    HomePage homePage;
    public HomePageSteps() {
        homePage = new HomePage(Hooks.driver);
    }

    @Given("^that Leo opened the Home page$")
    public void open_the_home_page() throws Exception{
        homePage.open(HomePage.URL);
    }
    @Then("^the Home page must contains only (\\d+) sliders$")
    public void the_home_page_must_contains_only_number_sliders(int number)throws Exception{
        homePage.showOnly(number).sliders();
    }
    @Then("^the Home page must contains only (\\d+) arrivals$")
    public void the_home_page_must_contains_only_number_arrivals(int number)throws Exception{
        homePage.showOnly(number).arrivals();
    }
}
