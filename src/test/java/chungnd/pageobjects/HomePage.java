package chungnd.pageobjects;

import chungnd.libs.BasePage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomePage extends BasePage {
    public HomePage(WebDriver driver) {
        super(driver);
    }

    public static String URL = "http://practice.automationtesting.in/";
    int number;
    @FindAll(@FindBy(css = "#n2-ss-6 > div.n2-ss-slider-1.n2-grab > div > div img"))
    public static List<WebElement> SLIDERS;

    @FindAll(@FindBy(css = ".woocommerce .products"))
    public static List<WebElement> ARRIVALS;

    public HomePage showOnly(int number) {
        this.number = number;
        return this;
    }

    public HomePage sliders() {
        Assert.assertEquals(SLIDERS.size(), number);
        return this;
    }

    public HomePage arrivals() {
        Assert.assertEquals(ARRIVALS.size(), number);
        return this;
    }
}
