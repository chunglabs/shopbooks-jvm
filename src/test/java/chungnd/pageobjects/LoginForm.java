package chungnd.pageobjects;

import chungnd.libs.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginForm extends BasePage {
    public LoginForm(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "")
    public static WebElement USERNAME_FIELD;

    @FindBy(css = "")
    public static WebElement PASSWORD_FIELD;

    @FindBy(css = "")
    public static WebElement LOGIN_BTN;

    public LoginForm with(String username){
        input(username).into(USERNAME_FIELD);
        return this;
    }
    public LoginForm and(String password){
        input(password).into(PASSWORD_FIELD);
        return this;
    }
    public LoginForm clickLoginBtn(){
        clickOn(LOGIN_BTN);
        return this;
    }
}
